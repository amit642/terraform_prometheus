resource "aws_instance" "jenkins" {
  ami           = "${var.ami_jen}"
  instance_type = "t2.micro"
security_groups = ["${var.sg}"]
subnet_id = "${var.subnet_id}"
associate_public_ip_address = "${var.pub_ip}"
key_name = "${var.key_name}"
iam_instance_profile = "${var.iam_instance_profile}"
user_data = <<-EOF
            #! /bin/bash
            sudo apt-get update
            sudo apt-get install python -y
            sudo apt install software-properties-common -y
            sudo apt-add-repository ppa:ansible/ansible
            sudo apt update
            sudo apt-get install ansible -y
            sudo apt-get install git -y
            sudo apt-get install awscli -y
            sudo apt-get install python-boto -y
            EOF
  tags = {
    Name = "${var.name}"
  }
}
