variable "ami_jen" {
    description = "ami of instance"
}
variable "sg" {
    description = "sg"
    type = "list"
}
variable "pub_ip" {
    default = "false"
}
variable "name" {
    description = "name pf instance for tag"
}
variable "subnet_id" {
    description = "subnet id in which ec2 launched"
}
variable "key_name" {
    description = "key name"
}
variable "iam_instance_profile" {
  description = "The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile."
  default     = ""
}