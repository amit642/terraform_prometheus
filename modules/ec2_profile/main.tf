resource "aws_iam_instance_profile" "ec2_profile" {
  name = "jenkins_ec2_profile"
  role = "${aws_iam_role.jenkins_role.name}"
}

resource "aws_iam_role" "jenkins_role" {
  name = "jenkins_server"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
tags = {
      tag-key = "jekins_acsess"
  }
}
resource "aws_iam_role_policy_attachment" "ec2-policy-attachment" {
    role = "${aws_iam_role.jenkins_role.name}"
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
