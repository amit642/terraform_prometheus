resource "aws_eip" "nat-eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat-eip.id}"
  subnet_id     = "${var.subnet_id}"
}