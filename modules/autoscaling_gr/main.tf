resource "aws_autoscaling_group" "pro_auto" {
  name                 = "prometheus"
  launch_configuration = "${var.launch_conf}"
  min_size             = 1
  max_size             = 3
  desired_capacity     = 2
  health_check_type    = "EC2"
  vpc_zone_identifier  = ["${var.sub-1}", "${var.sub-2}"]
}
