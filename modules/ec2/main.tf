resource "aws_instance" "web" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
security_groups = ["${var.sg}"]
subnet_id = "${var.subnet_id}"
associate_public_ip_address = "${var.pub_ip}"
key_name = "${var.key_name}"
user_data = <<-EOF
            #! /bin/bash
            sudo apt-get update
            sudo apt-get install python -y
            EOF
  tags = {
    Name = "${var.name}"
  }
}
