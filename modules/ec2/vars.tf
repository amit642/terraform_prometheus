variable "ami" {
    description = "ami of instance"
}
variable "sg" {
    description = "sg"
    type = "list"
}
variable "pub_ip" {
    default = "false"
}
variable "name" {
    description = "name pf instance for tag"
}
variable "subnet_id" {
    description = "subnet id in which ec2 launched"
}
variable "key_name" {
    description = "key name"
}