resource "aws_subnet" "subnet" {
  vpc_id     = "${var.vpc_id}"
  cidr_block = "${var.sub_cidr}"
availability_zone = "${var.az}"
tags = {
    name= "${var.sub_name}"
}
}
