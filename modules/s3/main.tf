resource "aws_s3_bucket" "key" {
  bucket = "myterra642"
  acl    = "private"

  tags = {
    Name        = "terra_bucket"
    Environment = "Dev"
  }
}
resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.key.id}"
  key    = "amit.pem"
  source = "${var.path_s3}"
  etag = "${filemd5("${var.path_s3}")}"
}
