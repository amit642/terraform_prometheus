resource "aws_vpc" "prometheus_infra" {
cidr_block = "${var.vpc_cidr}"
enable_dns_hostnames = "${var.enable_dns_hostnames}"
tags { 
name = "prometheus"
}
}


