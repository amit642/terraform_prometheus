resource "tls_private_key" "private_key" {
  count     = "${var.create ? 1 : 0}"
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_key_pair" "generated_pub_key" {
  count      = "${var.create ? 1 : 0}"
  key_name   = "${var.key_name}"
  public_key = "${tls_private_key.private_key.public_key_openssh}"
}

resource "aws_key_pair" "generating_key_pair" {
  count      = "${var.create ? 0 : 1}"
  key_name   = "${var.key_name}"
  public_key = "${file("${var.public_key_path}")}"
}
