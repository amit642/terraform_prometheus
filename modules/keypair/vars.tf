variable "key_name" {
  description = "specify the key_pair name"
}

variable "public_key_path" {
  description = "Specify the public Key path"
  default     = ""
}

variable "create" {
  description = "Specify FALSE if you have public key otherwise TRUE"
  default     = "TRUE"
}
