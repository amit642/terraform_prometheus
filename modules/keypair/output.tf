output "key_pair" {
  value = "${aws_key_pair.generated_pub_key.*.id}"
}

output "public_key" {
  value = "${aws_key_pair.generated_pub_key.*.public_key}"
}

output "private_key_pem" {
  value = "${tls_private_key.private_key.*.private_key_pem}"
}
