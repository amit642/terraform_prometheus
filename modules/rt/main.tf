resource "aws_route_table" "rt" {
  vpc_id = "${var.vpc_id}"

route {
    cidr_block = "${var.cidr_in}"
    gateway_id = "${var.gate_id}"
}
}