output "grafana_target_gr" {
  description = "ARN of the target group. Useful for passing to your Auto Scaling group module."
  value = "${aws_alb_target_group.grafana.arn}"
}
output "prometheus_target_gr" {
  description = "ARN of the target group. Useful for passing to your Auto Scaling group module."
  value = "${aws_alb_target_group.prometheus.arn}"
}
output "alb_id" {
  description = "ID of the generated ALB"
  value = "${aws_lb.alb.id}"
}
