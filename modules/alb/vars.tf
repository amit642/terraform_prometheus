variable "security_group_id" {
    description = "security group for alb"
}
variable "alb_subnets" {
    description = "subnets"
    type = "list"
}
variable "vpc_id" {

}

variable "auto_gr_name" {
    description = "auto scaling group name"
}
variable "jenkins_ec2_id" {
    description = "jenkins ec2 id"
}
variable "nginx_ec2_id" {
    description = "jenkins ec2 id"
}
variable "grafana_ec2_id" {
    description = "grafana ec2 id"
}