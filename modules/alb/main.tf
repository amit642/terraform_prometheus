resource "aws_lb" "alb" {
  name               = "alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${var.security_group_id}"]
  subnets            = ["${var.alb_subnets}"]
  enable_deletion_protection = false
}

resource "aws_alb_target_group" "grafana" {
    name                = "grafana"
    port                = "3000"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"
    target_type = "instance"
    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "302"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "garf-target-group"
    }
}
resource "aws_alb_target_group" "prometheus" {
    name                = "prometheus"
    port                = "9090"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"
    target_type = "instance"
    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "302"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "prom-target-group"
    }
}
resource "aws_alb_target_group" "jenkins" {
    name                = "jenkins"
    port                = "8080"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"
    target_type = "instance"
    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "302"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "jenkins-target-group"
    }
}
resource "aws_alb_target_group" "nginx" {
    name                = "nginx"
    port                = "80"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"
    target_type = "instance"
    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "302"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "nginx-target-group"
    }
}
resource "aws_alb_listener" "nginx" {
    load_balancer_arn = "${aws_lb.alb.id}"
    port              = "80"
    protocol          = "HTTP"

    default_action {
        target_group_arn = "${aws_alb_target_group.nginx.arn}"
        type             = "forward"
    }
}
resource "aws_alb_listener" "grafana" {
    load_balancer_arn = "${aws_lb.alb.id}"
    port              = "3000"
    protocol          = "HTTP"

    default_action {
        target_group_arn = "${aws_alb_target_group.grafana.arn}"
        type             = "forward"
    }
}

resource "aws_alb_listener" "prometheus" {
    load_balancer_arn = "${aws_lb.alb.id}"
    port              = "9090"
    protocol          = "HTTP"

    default_action {
        target_group_arn = "${aws_alb_target_group.prometheus.arn}"
        type             = "forward"
    }
}
resource "aws_alb_listener" "jenkins" {
    load_balancer_arn = "${aws_lb.alb.id}"
    port              = "8080"
    protocol          = "HTTP"

    default_action {
        target_group_arn = "${aws_alb_target_group.jenkins.arn}"
        type             = "forward"
    }
}
resource "aws_lb_target_group_attachment" "jenkins" {
  target_group_arn = "${aws_alb_target_group.jenkins.arn}"
  target_id        = "${var.jenkins_ec2_id}"
}
resource "aws_lb_target_group_attachment" "nginx" {
  target_group_arn = "${aws_alb_target_group.nginx.arn}"
  target_id        = "${var.nginx_ec2_id}"
}
resource "aws_lb_target_group_attachment" "grafana" {
  target_group_arn = "${aws_alb_target_group.grafana.arn}"
  target_id        = "${var.grafana_ec2_id}"
}
resource "aws_autoscaling_attachment" "asg_attachment_prom" {
  autoscaling_group_name = "${var.auto_gr_name}"
  alb_target_group_arn   = "${aws_alb_target_group.prometheus.arn}"
}

