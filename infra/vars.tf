variable "aws_region" {
description = "AWS region "
default = "us-east-1"
}
variable "shared_cred_file" {
 default = "/home/jenkins/.aws/credentials"
}
variable "vpc_cidr" {
description = "CIDR of VPC "
default = "10.0.0.0/16"
}

variable "sub_pub_a_cidr" {
    description = "CIDR of public subnets"
    default = "10.0.1.0/24"
}
variable "sub_pub_a_az" {
    description = "avaibility zone of public subnets"
    default = "us-east-1a"
}
variable "sub_pub_b_cidr" {
    description = "CIDR of public subnets"
    default = "10.0.2.0/24"
}
variable "sub_pub_b_az" {
    description = "avaibility zone of public subnets"
    default = "us-east-1b"
}
variable "sub_pri_a_cidr" {
    description = "CIDR of public subnets"
    default = "10.0.3.0/24"
}
variable "sub_pri_a_az" {
    description = "avaibility zone of private a  subnets"
    default = "us-east-1a"
}
variable "sub_pri_b_cidr" {
    description = "CIDR of public subnets"
    default = "10.0.4.0/24"
}
variable "sub_pri_b_az" {
    description = "avaibility zone of private b subnets"
    default = "us-east-1b"

}

variable "key_name" {
  description = "specify the key_pair name"
  default = "ninja"
}

variable "public_key_path" {
  description = "Specify the public Key path"
  default = "/home/amit/.ssh/id_rsa.pub"
}

variable "create" {
  description = "Specify FALSE if you have public key otherwise TRUE"
  default = "TRUE"  
}
variable "instance_ami" {
    description = "instance ami for ubuntu-16"
    default = "ami-0a313d6098716f372"
}
variable "jenkins_ami" {
    description = "jenkins ami"
    default = "ami-075af8d203327435f"
}
