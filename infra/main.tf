provider "aws" {
region = "${var.aws_region}"
 version = "~> 2.9.0"
shared_credentials_file = "${var.shared_cred_file}"
 profile = "default"
}

module "vpc" {
source = "../modules/vpc"
vpc_cidr = "${var.vpc_cidr}"
}

module "pub_a" {
source = "../modules/subnet"
vpc_id = "${module.vpc.vpc_id}"
sub_cidr = "${var.sub_pub_a_cidr}"
az = "${var.sub_pub_a_az}"
sub_name = "public-a"
}
module "pub_b" {
source = "../modules/subnet"
vpc_id = "${module.vpc.vpc_id}"
sub_cidr = "${var.sub_pub_b_cidr}"
az = "${var.sub_pub_b_az}"
sub_name = "public-b"
}
module "pri_a" {
source = "../modules/subnet"
vpc_id = "${module.vpc.vpc_id}"
sub_cidr = "${var.sub_pri_a_cidr}"
az = "${var.sub_pri_a_az}"
sub_name = "private-a"
}
module "pri_b" {
source = "../modules/subnet"
vpc_id = "${module.vpc.vpc_id}"
sub_cidr = "${var.sub_pri_b_cidr}"
az = "${var.sub_pri_b_az}"
sub_name = "private-b"
}
module "igw" {
source = "../modules/ig"
vpc_id = "${module.vpc.vpc_id}"
}

module "nat" {
    source = "../modules/nat"
    subnet_id = "${module.pub_a.subnet_id}"
}

module "route_table_public" {
    source = "../modules/rt"
    vpc_id = "${module.vpc.vpc_id}"
    cidr_in = "0.0.0.0/0"
    gate_id = "${module.igw.igw_id}"
}
module "route_table_private" {
    source = "../modules/rt"
    vpc_id = "${module.vpc.vpc_id}"
    cidr_in = "0.0.0.0/0"
    gate_id = "${module.nat.nat_id}"
}

module "sub_asso_rt_pub_a" {
    source = "../modules/sub_asso_rt"
    rt_id = "${module.route_table_public.rt_id}"
    subnet_asso_id = "${module.pub_a.subnet_id}"
 }
module "sub_asso_rt_pub_b" {
    source = "../modules/sub_asso_rt"
    rt_id = "${module.route_table_public.rt_id}"
    subnet_asso_id = "${module.pub_b.subnet_id}"
 }
module "sub_asso_rt_pri_a" {
    source = "../modules/sub_asso_rt"
    rt_id= "${module.route_table_private.rt_id}"
    subnet_asso_id = "${module.pri_a.subnet_id}"
}
module "sub_asso_rt_pri_b" {
    source = "../modules/sub_asso_rt"
    rt_id= "${module.route_table_private.rt_id}"
    subnet_asso_id = "${module.pri_b.subnet_id}"
}

module "sg" {
source = "../modules/sg"
vpc_id = "${module.vpc.vpc_id}"
}

module "key_pair" {
  source          = "../modules/keypair"
  key_name        = "${var.key_name}"
  create          = "${var.create}"
 public_key_path = "${var.public_key_path}"
}

module "s3_bucket" {
    source = "../modules/s3"
}

module "aws_ec2_profile"{
    source = "../modules/ec2_profile"
}
module "ec2-jump" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pub_a.subnet_id}"
    key_name = "amit"
    pub_ip = "true"
    name = "jump"
}
module "ec2-1" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "mysql"
}
module "ec2-2" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "nginx"
}
module "ec2-3" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "apache"
}
module "ec2-4" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "node"
}
module "ec2-jen" {
    source = "../modules/ec2_jen"
    ami_jen = "${var.jenkins_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    iam_instance_profile = "${module.aws_ec2_profile.aws_profile}"
    key_name = "amit"
    name = "jenkins"
}
module "ec2-5" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "timesdb"
}
module "ec2-6" {
    source = "../modules/ec2"
    ami = "${var.instance_ami}"
    sg = ["${module.sg.sg_id}"]
    subnet_id = "${module.pri_a.subnet_id}"
    key_name = "amit"
    name = "grafana"
}
module "launch_config" {
    source = "../modules/launch_conf"
    ami = "${var.instance_ami}"
    sg = "${module.sg.sg_id}"
    keypair = "amit"
}

module "autoscaling" {
    source = "../modules/autoscaling_gr"
    launch_conf = "${module.launch_config.launch_id}"
    sub-1 = "${module.pri_a.subnet_id}"
    sub-2 = "${module.pri_b.subnet_id}"
}
module "alb" {
    source = "../modules/alb"
    vpc_id = "${module.vpc.vpc_id}"
    alb_subnets = ["${module.pub_a.subnet_id}","${module.pub_b.subnet_id}"]
security_group_id = "${module.sg.sg_id}"
auto_gr_name = "${module.autoscaling.pro_autoscaling_id}"
jenkins_ec2_id = "${module.ec2-jen.ec2_id}"
nginx_ec2_id = "${module.ec2-2.ec2_id}"
grafana_ec2_id = "${module.ec2-6.ec2_id}"
}
